console.log("Hello, B248!");

//Objects

/*
	An object is a data type that is used to represent real world objects
	**It is also a collection of related data and or functionalities

	Information stored in objects are represented in a key:value pair

	"key" - "property" of an object
	"value" - actual data to be stored

	**different data type may be stored in an object's property creating complex data structures

*/

//2 ways in creating objects in JS

//1. Object Literal Notation/
	//let object = {}; (curly braces)
//2. Object Constructor Notation/
	//Object Instantation
	//let object = new Object()


//Object Literal Notation
	//creating objects using initializer/literal notation
	//camelCase
	//A cellphone is an example of a real world object
	//It has it's own properties such as name, color, weight, unit model and a lot of other things
	/*
		Syntax:

		let objectName = {
			keyA : valueA,
			keyB : valueB
		};
	*/

	
	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	};

	console.log("Result from creating objects using literal notation: ");
	console.log(cellphone);

	let cellphone2 = {
		name: "iPhone 14",
		manufactureDate: 2023
	}

	console.log("Result from creating objects using literal notation: ");
	console.log(cellphone2);

	let ninja = {
		name: "Naruto Uzumaki",
		village: "Konoha",
		children: ["Boruto", "Himawari"]
	};
	console.log(ninja);

	/*

		Mini Activity
		create an object called "bootcamper" (3 min 6:50PM)

		Your:

		name
		address
		hobbies
		favorite movie
		note to self

	*/

	let bootcamper = {
		name: "Cee",
		address: "South Korea",
		hobbies: ["Playing LOL","Digital Art"],
		favoriteMovie: "The Pianist",
		noteToSelf: "'di lang kakayanin, kayang-kaya!"

	}

	console.log(bootcamper);

	//Object Constructor Notation
		//Creating objects using a constructor function
		//creates a REUSABLE function to create several objects that have the same data structure
		//This is useful for creating multiple instances/copies of an object
		//An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it

		/*
			Syntax

			function objectName(keyA,keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}
		*/

			//this keyword refers to the properties within the object
				//it allows the assignment of new objects' properties by associating them with the values received from the constructor function's parameter



			//constructor serves a blueprint
			function Laptop(name,manufactureDate){
				this.name = name;
				this.manufactureDate = manufactureDate;
			}

			//create an instance object using the Laptop constructor

			let laptop = new Laptop("Lenovo", 2008);

			console.log("Result from creating objects using object constructor");
			console.log(laptop);

			//the "new" operator creates an instance of an object (new object)
			let myLaptop = new Laptop("Macbook Air", 2020);

			console.log("Result from creating objects using object constructor");
			console.log(myLaptop);

			//Mini Activity #2
			//create 3 more instances of our Laptop constructor

			let laptop2 = new Laptop("Asus", 2023);
			console.log(laptop2);

			let laptop3 = new Laptop("Dell", 2027);
			console.log(laptop3);

			let laptop4 = new Laptop("Acer", 2025);
			console.log(laptop4);


			//without the new keyword

			let oldLaptop = Laptop("Portal R2E CCMC",1980);
			console.log("Result from creating objects using object constructor");
			console.log(oldLaptop);//undefined
			
			//Create empty objects

			let computer = {};
			let myComputer = new Object();

			console.log(computer);
			console.log(myComputer);


// Accessing Object Properties

			// 1. dot notation
				console.log("Result from the dot notation: " + myLaptop.name);
			// 2. square bracket notation
				console.log("Result from the square bracket notation: " + myLaptop["name"]);

				console.log(ninja.name);
				console.log(ninja.village);
				console.log(ninja.children[1]);


// Accessing array of objects
			// accessing object properties using the square bracket notation and array indexes can cause confusion


			let arrayObj = [laptop,myLaptop];
				// may be confused for accessing array indexes
			console.log(arrayObj[0]["name"]);
				// this tells us that arrays[0] is an object by using the dot notation
			console.log(arrayObj[0].name);

// Initialize/Add/Delete/Reassign Object Properties
/*
	While using the square bracket will allow access to space when assigning property names to make it easier to read, this also makes it so that the object properties can only be accessed using the square bracket notation
	This also makes names of object properties to not follow commonly used naming conventions for them

*/

// create am object using object literals

let car = {};
console.log("Current value of car object");
console.log(car);// {}

// initializing a value/ adding object properties
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

car["manufacture date"] = 2019;
console.log("Result from adding properties using square bracket notation: ");
console.log(car);

delete car["manufacture date"];
console.log("Result from deleting properties");
console.log(car);

car["manufactureDate"] = 2019;
console.log(car);

// Reassigning object property values

car.name = "Toyota Vios";
console.log(car);

delete car.manufactureDate;
console.log(car);


// Object Methods
	// a method is a function which is a property of an object

let person = {
	name: "Cardo Dalisay",
	talk: function(){
		console.log("Hello, my name is " + this.name);
	}
}
console.log(person);
console.log("Result from object method: ");
person.talk();

person.walk = function(steps){
	console.log(this.name + " walked " + steps + " steps forward");
}
console.log(person);
person.walk(500);

let friend = {

	firstName: "Tom",
	lastName: "Sawyer",
	address: {
		city: "Manila",
		country: "Philippines"
	},
	emails: ["sawyer@mail.com","tomsawyer@mail.com"],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + " " + this.lastName + ". " + " I live in " + this.address.city + ", " + this.address.country)
	}
}

friend.introduce();

// REAL WORLD APPLICATION OF APPLICATIONS
/*
	1. We 
*/

function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;

		console.log(target.name + " health is reduced to " + target.health);

		if(target.health <= 0){
			target.faint();
		}else{
			console.log(target.name + "'s health is " + target.health);
		}
	}

	this.faint = function(){
		console.log(this.name + " has fainted.");
	}
}

let pikachu = new Pokemon ("Pikachu", 80);
console.log(pikachu);
let rattata = new Pokemon ("Rattata", 100);
console.log(rattata);

pikachu.tackle(rattata);