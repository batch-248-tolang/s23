console.log("Who's that pokemon?")

let trainer = {
	name: "Ash Tray",
	age: 69,
	pokemon: ["Pikachu","Rattata","Charizard","Mew"],
	friends: {
		Ianna: ["Eeve","Snorlax"],
		Myles: ["Ditto","Lucario"],
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;

		console.log(target.name + "'s health is reduced to " + target.health);

		if(target.health <= 0){
			target.faint();
		}else{
			console.warn(target.name + " is still alive");
		}
	}

	this.faint = function(){
		console.log(this.name + " has fainted.");
	}
}
let pikachu = new Pokemon ("Pikachu", 98);
console.log(pikachu);

let rattata = new Pokemon ("Rattata", 50);
console.log(rattata);

let charizard = new Pokemon ("Charizard", 123);
console.log(charizard);

charizard.tackle(pikachu);

console.log(pikachu);

charizard.tackle(rattata);

console.log(rattata);